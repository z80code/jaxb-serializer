JAXB practical task

Create maven project jaxb-serializer that consists of 2 sub-modules:
    jaxb-serializer-bindings and jxb-serializer-impl.

| jaxb-serializer-bindings

    Contains XSD schema that describes the structure of data intended
    to store catalog of products.

    The root element of the catalog is products (<xs:element name="products">) –
    complex type that contains unbounded amount of product elements
    (<xs:element minOccurs="1" name="product" maxOccurs="unbounded">).

    Each of product elements consists of a set of tags:
    –	name (xs:string)
    –	price (xs:integer)
    –	amount (xs:integer)
    –	description (xs:string)
    –	type (element of type ProductType – enumeration,
        supported values convenience, shopping, speciality, unsought)
    This module should generate Java classes based on XSD schema (use JAXB maven plugin).

| jaxb-serializer-impl

    Contains functionality that provides an ability to marshal and unmarshall XML files
    and map them to created XSD schema.

    Please note, this module must contain jaxb-bindings module in a list of dependencies.

    Create Main class with method main to demonstrate how the modules works.
    The class  receives from the console following parameters:
    –	in (path to input XML file, URL)
    –	out (path to output XML file, URL)
    The class unmarshalls the XML file (–in parameter),
    creates the list of products and prints it to console.
    After that class marshalls the list of products to xml file  specified by -out parameter.
    Module should be packed as runnable JAR
    (MANIFEST.MF should contain Main-Class: com.epam.training.Main)
