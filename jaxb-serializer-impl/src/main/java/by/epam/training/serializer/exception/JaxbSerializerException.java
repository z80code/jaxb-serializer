package by.epam.training.serializer.exception;

public class JaxbSerializerException extends Exception {
	public JaxbSerializerException() {
	}

	public JaxbSerializerException(String message) {
		super(message);
	}

	public JaxbSerializerException(String message, Throwable cause) {
		super(message, cause);
	}
}
