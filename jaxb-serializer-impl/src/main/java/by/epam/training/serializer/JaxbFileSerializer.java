package by.epam.training.serializer;

import by.epam.training.jaxb.ObjectFactory;
import by.epam.training.jaxb.Product;
import by.epam.training.jaxb.Products;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import java.io.File;
import java.util.List;

/**
 * This Class contains functionality that provides an ability
 * to marshal and unmarshall XML files and map them to created XSD schema.
 */
public class JaxbFileSerializer {
	/**
	 * The method to unmarshalling XML files
	 * @param file - to unmarshall.
	 * @param schema - to validate.
	 * @return - List of products.
	 * @throws JAXBException - in case of the errors of a parsing and ect.
	 */
	public static List<Product> unmarshall(File file, Schema schema) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Products.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		jaxbUnmarshaller.setSchema(schema);
		Products products = (Products) jaxbUnmarshaller.unmarshal(file);
		if(products==null){
			throw new RuntimeException("Can`t to get the products.");
		}
		return products.getProduct();
	}

	/**
	 * The method to marshalling XML files
	 * @param productsList - List of products.
	 * @param file - to marshall.
	 * @throws JAXBException - in case of the errors of a parsing and ect.
	 */
	public static void marshal(List<Product> productsList, File file) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Products.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		Products products = new ObjectFactory().createProducts();
		products.getProduct().addAll(productsList);
		jaxbMarshaller.marshal(products, file);
	}
}
