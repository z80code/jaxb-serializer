package by.epam.training;

import by.epam.training.jaxb.Product;
import by.epam.training.serializer.JaxbFileSerializer;
import by.epam.training.validator.ArgsValidator;

import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.util.List;


public class Runner {

	private static final String SCHEMA_LOCATION = "products.xsd";

	public static void main(String[] args) {
		try {
			ArgsValidator.validate(args);

			Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
					.newSchema(new File(SCHEMA_LOCATION));

			List<Product> products = JaxbFileSerializer.unmarshall(new File(args[0]), schema);

			productsPrint(products);

			JaxbFileSerializer.marshal(products, new File(args[1]));

		} catch (Exception e) {
			// The Top lvl for an Exception Handling.
			// I choose only a print of an error message.
			System.out.println(e.toString());
		}
	}

	private static final String PRODUCT_TABLE_CAP_TEMPLATE =
			"| Product |      Description     | Amount |  Price |        Type      |";
	private static final String PRODUCT_TABLE_LINE_TEMPLATE =
			"-----------------------------------------------------------------------";
	private static void productsPrint(List<Product> products) {
		System.out.println(PRODUCT_TABLE_LINE_TEMPLATE);
		System.out.println(PRODUCT_TABLE_CAP_TEMPLATE);
		System.out.println(PRODUCT_TABLE_LINE_TEMPLATE);
		products.forEach(Runner::productPrint);
		System.out.println(PRODUCT_TABLE_LINE_TEMPLATE);
	}

	private static final String PRODUCT_PRINT_TEMPLATE = "| %7s | %20s | %6s | %6s | %16s |\n";

	private static void productPrint(Product product) {
		System.out.printf(PRODUCT_PRINT_TEMPLATE,
				product.getName(),
				product.getDescription(),
				product.getAmount(),
				product.getPrice(),
				product.getType());
	}
}
