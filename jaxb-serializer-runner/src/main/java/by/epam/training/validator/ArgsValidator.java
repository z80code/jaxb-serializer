package by.epam.training.validator;

/**
 * Args validate in this class.
 */
public class ArgsValidator {
	/**
	 * Check the args for a valid.
	 * In case of error it will throw an Exception.
	 * @param args Array of args.
	 */
	public static void validate(String... args) {

		if (
				(args.length < 2) ||
				(args[0] == null || args[0].equals("")) ||
				(args[1] == null || args[1].equals(""))
				) {
			throw new RuntimeException("Arguments count are not correct.");
		}
	}
}
