package by.epam.training.validator;

import org.junit.Test;

import static org.junit.Assert.*;

public class ArgsValidatorTest {

	@Test(expected = RuntimeException.class)
	public void validateErrorCount() {

		String[] errorArgsCount = new String[]{
				"first"
		};
		ArgsValidator.validate(errorArgsCount);
	}

	@Test(expected = RuntimeException.class)
	public void validateErrorEmptyFirst() {

		String[] errorArgsEmptyFirst = new String[]{
				"",
				"second"
		};

		ArgsValidator.validate(errorArgsEmptyFirst);
	}


	@Test(expected = RuntimeException.class)
	public void validateErrorEmptySecond() {

		String[] errorArgsEmptySecond = new String[]{
				"first",
				""
		};
		ArgsValidator.validate(errorArgsEmptySecond);

	}

	@Test
	public void validate() {

		String[] args = new String[]{
				"first",
				"second"
		};

		ArgsValidator.validate(args);
	}
}